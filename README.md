# k8s-dashboards

Grafana based dashboards for monitoring k8s clusters and pods.

## Dashboards created for following monitoring stack
- [Prometheus](https://prometheus.io/)
- [Thanos](https://thanos.io/)
- [Grafana](https://grafana.com/) `v8.4+`

## Prereqs
- [go](https://go.dev/dl/) `v1.17+`
- [go-jsonnet](https://github.com/google/go-jsonnet)
- [jsonnet-bundler](https://github.com/jsonnet-bundler/jsonnet-bundler/)
- [grafonnet](https://github.com/grafana/grafonnet)

## Grafana Plugins required
- [grafana polystat panel](https://github.com/grafana/grafana-polystat-panel)

## Setup
Install go-jsonnet library 
```
go install github.com/google/go-jsonnet/cmd/jsonnet@latest
```

Install jsonnet-bundler
```
go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@latest
```

Add jb and jsonnet to your PATH
```
export PATH=$PATH:$(go env GOPATH)/bin
```

Install grafonnet
```
jb install github.com/grafana/grafonnet/gen/grafonnet-latest@main
```

Install grafonnet polystat pannel plugin extension
```
jb install github.com/thelastpickle/grafonnet-polystat-panel@master
```

## Generate dashboard
```bash
cd templates
jsonnet -J vendor/ cluster-health/cluster_health_dash.jsonnet
```
