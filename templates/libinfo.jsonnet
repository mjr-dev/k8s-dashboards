local g = import 'github.com/grafana/grafonnet/gen/grafonnet-v10.2.0/main.libsonnet';

// Grafonnet components
local dashboard = g.dashboard;
local variable = dashboard.variable;
local row = g.panel.row;
local panel = g.panel;
local query = g.query;
local util = g.util;
local stat = g.panel.stat;
local gauge = g.panel.gauge;
local table = g.panel.table;

// List properties
local statFields = std.objectFields(stat);
local statOptions = std.objectFields(stat.options);
local statQueryOptions = std.objectFields(stat.queryOptions);
local gaugeFields = std.objectFields(gauge);
local gaugeOptions = std.objectFields(gauge.options);
local gaugePanelOptions = std.objectFields(gauge.panelOptions);
local gaugeQueryOptions = std.objectFields(gauge.queryOptions);
local gaugeStandardOptions = std.objectFields(gauge.standardOptions);
local gaugeColor = std.objectFields(gauge.standardOptions.color);
local gaugeThresholds = std.objectFields(gauge.standardOptions.thresholds);
local tableFields = std.objectFields(table);
local tableOptions = std.objectFields(table.options);
local tablePanelOptions = std.objectFields(table.panelOptions);
local tableQueryOptions = std.objectFields(table.queryOptions);
local tableStandardOptions = std.objectFields(table.standardOptions);
local tableFooter = std.objectFields(table.options.footer);
local varFields = std.objectFields(variable);

{
    stat: {
        fields: statFields,
        options: statOptions,
        queryOptions: statQueryOptions
    },
    gauge: {
        fields: gaugeFields,
        options: gaugeOptions,
        panelOptions: gaugePanelOptions,
        queryOptions: gaugeQueryOptions,
        standardOptions: gaugeStandardOptions,
        color: gaugeColor,
        thresholds: gaugeThresholds
    },
    table: {
        fields: tableFields,
        options: tableOptions,
        footer: tableFooter,
        panelOptions: tablePanelOptions,
        queryOptions: tableQueryOptions,
        standardOptions: tableStandardOptions
    },
    variable: {
        fields: varFields
    }
}