local grafonnet = import 'github.com/grafana/grafonnet/gen/grafonnet-v10.2.0/main.libsonnet';
local prometheusQuery = grafonnet.query.prometheus;
local vars = import './variables.jsonnet';
local datasourceName = '$' + vars.datasource.name;

local readyNodesQuery = 'sum(kube_node_status_condition{condition="Ready", status="true", cluster_name=~"$cluster"})';

/************************************************
 * Resource usage stat queries
 * Calculating usage:
 * usagePercentage = (used) / (total) * 100
 ************************************************/
local usedCoresQuery = 'sum(rate(container_cpu_usage_seconds_total{id="/", node=~"$node", cluster_name=~"$cluster"}[1m]))';
local totalCoresQuery = 'sum(machine_cpu_cores{node=~"$node", cluster_name=$cluster})';
local cpuStatQuery = usedCoresQuery + ' / ' + totalCoresQuery + ' * 100';

local usedMemQuery = 'sum(container_memory_working_set_bytes{id="/", node=~"$node", cluster_name=~"cluster"})';
local totalMemQuery = 'sum(machine_memory_bytes{node=~"$node", cluster_name=$cluster})';
local memStatQuery = usedMemQuery + ' / ' + totalMemQuery + ' * 100';

local usedDiskQuery = 'sum(container_fs_usage_bytes{device=~"^/dev/[sv]d[a-z][1-9]$", id="/", node=~"$node", cluster_name=~"$cluster"})';
local totalDiskQuery = 'sum(container_fs_limit_bytes{device=~"^/dev/[sv]d[a-z][1-9]$", id="/", node=~"$node", cluster_name=~"$cluster"})';
local diskStatQuery = usedDiskQuery + ' / ' + totalDiskQuery + ' * 100';

/*************************
* Pod related queries
* Calculating Pod usage for cluster:
* usagePercentage = (totalPods) / (podCapacity) * 100
**************************/
local totalPodsQuery = 'sum(kube_pod_info{cluster_name=~"$cluster"})';
local podCapacityQuery = 'sum(kube_node_status_allocatable{resource="pods", cluster_name=~"$cluster"})';
local podUsageQuery = totalPodsQuery + ' / ' + podCapacityQuery + ' * 100';
local runningPodsQuery = 'sum(kube_pod_status_phase{phase="Running", cluster_name=~"$cluster"})';
local pendingPodsQuery = 'sum(kube_pod_status_phase{phase="Pending", cluster_name=~"$cluster"})';
local failedPodsQuery = 'sum(kube_pod_status_phase{phase="Failed", cluster_name=~"$cluster"})';
local unhealthyPodsQuery = 'sum by (cluster_name,namespace,pod,container,reason)(kube_pod_container_status_waiting_reason{cluster_name=~"$cluster", reason=~"CrashLoopBackOff|ImagePullBackOff"})';

// Dashboard Panel query definitions
{
    /*****************
    * Cluster health
     ****************/
    // Getting all ready nodes in cluster
    totalReadyNodes:
        prometheusQuery.new(
            datasourceName,
            readyNodesQuery,
        ),
    
    /**************************
    * Cluster resource usage
     **************************/
    // CPU
    cpuStats:
        prometheusQuery.new(
            datasourceName,
            cpuStatQuery,
        ),
    usedCores:
        prometheusQuery.new(
            datasourceName,
            usedCoresQuery,
        ),
    totalCores:
        prometheusQuery.new(
            datasourceName,
            totalCoresQuery,
        ),
    // MEMORY
    memStats:
        prometheusQuery.new(
            datasourceName,
            usedMemQuery,
        ),
    usedMem:
        prometheusQuery.new(
            datasourceName,
            usedMemQuery,
        ),
    totalMem:
        prometheusQuery.new(
            datasourceName,
            totalMemQuery,
        ),
    // DISK
    diskStats:
        prometheusQuery.new(
            datasourceName,
            diskStatQuery,
        ),
    usedDisk:
        prometheusQuery.new(
            datasourceName,
            usedDiskQuery,
        ),
    totalDisk:
        prometheusQuery.new(
            datasourceName,
            totalDiskQuery,
        ),
    
    /**************************
    * Pod Stats
     **************************/
    podUsageStats:
        prometheusQuery.new(
            datasourceName,
            podUsageQuery,
        ),
    totalPods:
        prometheusQuery.new(
            datasourceName,
            totalPodsQuery,
        ),
    podMaxCapacity:
        prometheusQuery.new(
            datasourceName,
            podCapacityQuery,
        ),
    totalRunningPods:
        prometheusQuery.new(
            datasourceName,
            runningPodsQuery,
        ),
    totalPendingPods:
        prometheusQuery.new(
            datasourceName,
            pendingPodsQuery,
        ),
    totalFailedPods:
        prometheusQuery.new(
            datasourceName,
            failedPodsQuery,
        ),
    unhealthyPods:
        prometheusQuery.new(
            datasourceName,
            unhealthyPodsQuery,
        ),
}