local grafonnet = import 'github.com/grafana/grafonnet/gen/grafonnet-v10.2.0/main.libsonnet';
local vars = import './variables.jsonnet';
local varDatasource = '${' + vars.datasource.name + '}';

{
    stat: {
        local stat = grafonnet.panel.stat,
        local options = stat.options,
        local queryOptions = stat.queryOptions,
        local standardOptions = stat.standardOptions,
        local statDatasource = stat.datasource,

        base(title, targets):
            stat.new(title)
            + stat.queryOptions.withTargets(targets)
            + statDatasource.withUid(varDatasource)
            + options.withGraphMode(value="none"),
        // Base panel for generic stats: no color gradients
        staticStat(title, targets):
            self.base(title, targets)
            + options.withColorMode(value="none")
            + options.withJustifyMode(value="auto")
            + options.withTextMode(value="auto")
            + options.withOrientation(value="auto")
            + options.withReduceOptions(value={
                "values": false,
                "calcs": [
                    "lastNotNull"
                ],
                "fields": ""
            }),
        // Static stat panel with byte units
        bytes(title, targets):
            self.staticStat(title, targets)
            + standardOptions.withUnit("bytes")
            + standardOptions.withDecimals(2),
        // Static stat panel with cpu core units
        cores(title, targets):
            self.staticStat(title, targets)
            + standardOptions.withUnit("cores")
            + standardOptions.withDecimals(2),
        // ReadyNodes panel
        readyNodes(title, targets):
            self.base(title, targets),
        // Pod Related stat panels
        runningPodsStat(title, targets):
            self.staticStat(title, targets)
            + options.withColorMode(value="background_solid")
            + standardOptions.thresholds(value={
                "mode": "absolute",
                "steps": [
                    {
                        "color": "green",
                        "value": null
                    }
                ]
            }),
        pendingPodsStat(title, targets):
            self.staticStat(title, targets)
            + options.withColorMode(value="background_solid")
            + standardOptions.thresholds(value={
                "mode": "absolute",
                "steps": [
                    {
                        "color": "dark-yellow",
                        "value": null
                    }
                ]
            }),
        failedPodsStat(title, targets):
            self.staticStat(title, targets)
            + options.withColorMode(value="background_solid")
            + standardOptions.thresholds(value={
                "mode": "absolute",
                "steps": [
                    {
                        "color": "red",
                        "value": null
                    }
                ]
            }),
    },

    table: {
        local table = grafonnet.panel.table,
        local options = table.options,
        local queryOptions = table.queryOptions,
        local standardOptions = table.standardOptions,
        local override = standardOptions.override,
        local tableDatasource = table.datasource,

        base(title, targets):
            table.new(title)
            + table.queryOptions.withTargets(targets)
            + tableDatasource.withUid(varDatasource)
            + options.withShowHeader(value=true)
            + options.withCellHeight(value="md")
            + options.footer.TableFoooterOptions.withCountRows(value=false)
            + options.footer.TableFoooterOptions.withEnablePagination(value=true)
            + options.footer.TableFoooterOptions.withReducer(value="sum")
            + options.footer.TableFoooterOptions.withShow(value=false),
        // Base panel with timestamp field removed
        baseNoTime(title, targets):
            self.base(title, targets)
            + table.standardOptions.withOverrides([
                override.matcher.withId('byName')
                + override.matcher.withOptions(value='Time')
                + override.withProperties([
                    override.properties.withId('custom.hidden')
                    + override.properties.withValue(value=true)
                ])
            ]),
        // Base panel with value field removed
        baseNoValue(title, targets):
            self.base(title, targets)
            + standardOptions.withOverrides([
                override.matcher.withId('byName')
                + override.matcher.withOptions(value='Value')
                + override.withProperties([
                    override.properties.withId('custom.hidden')
                    + override.properties.withValue(value=true)
                ])
            ]),
        // Unhealthy pods table panel
        unhealthyPods(title, targets):
            self.baseNoValue(title, targets)
            + standardOptions.withFilterable(value=true),
    },

    gauge: {
        local gauge = grafonnet.panel.gauge,
        local options = gauge.options,
        local standardOptions = gauge.standardOptions,
        local queryOptions = gauge.queryOptions,
        local gaugeDatasource = gauge.datasource,

        base(title, targets):
            gauge.new(title)
            + gauge.queryOptions.withTargets(targets)
            + gaugeDatasource.withUid(varDatasource)
            + gauge.options.withReduceOptions(value={
                "values": false,
                "calcs": [
                    "lastNotNull"
                ],
                "fields": ""
            })
            + gauge.options.withOrientation(value="auto")
            + gauge.options.withShowThresholdLabels(value=false)
            + gauge.options.withShowThresholdMarkers(value=true)
            + gauge.standardOptions.color.withMode(value="thresholds")
            + gauge.standardOptions.thresholds.withMode(value="absolute")
            + gauge.standardOptions.thresholds.withSteps(value=[
                {
                    "color": "rgba(50, 172, 45, 0.97)",
                    "value": null
                },
                {
                    "color": "rgba(237, 129, 40, 0.89)",
                    "value": 55
                },
                {
                    "color": "rgba(245, 54, 54, 0.9)",
                    "value": 80
                },
            ])
            + gauge.standardOptions.withMax(100)
            + gauge.standardOptions.withMin(0)
            + gauge.standardOptions.withUnit("percent"),
        
        cpuCoreUsage(title, targets):
            self.base(title, targets),

        memUsage(title, targets):
            self.base(title, targets),
            
        diskUsage(title, targets):
            self.base(title, targets),
    },
}