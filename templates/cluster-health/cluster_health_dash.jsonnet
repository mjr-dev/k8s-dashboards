local grafonnet = import 'github.com/grafana/grafonnet/gen/grafonnet-v10.2.0/main.libsonnet';
local panels = import './panels.jsonnet';
local vars = import './variables.jsonnet';
local queries = import './queries.jsonnet';
local row = grafonnet.panel.row;
local tags = ['kubernetes', 'monitoring', 'v0.0.1'];

grafonnet.dashboard.new('Cluster Health Summary')
+ grafonnet.dashboard.withDescription(|||
  Summary metrics dashboard related to cluster health. Shows overall cluster CPU/Memory/Disk usage and pod conditions.
|||)
+ grafonnet.dashboard.withVariables([
  vars.datasource,
  vars.cluster,
  vars.node,
])
+ grafonnet.dashboard.withTags(tags)
+ grafonnet.dashboard.withTimezone(value="browser")
+ grafonnet.dashboard.time.withFrom(value="now-2h")
+ grafonnet.dashboard.time.withTo(value="now")
+ grafonnet.dashboard.withEditable(value=true)
+ grafonnet.dashboard.withPanels(
    grafonnet.util.grid.makeGrid([
        row.new('Node Readiness Status')
        + row.withPanels([
            panels.stat.readyNodes('Node Health', queries.totalReadyNodes)
        ]),
        row.new('Cluster Usage Stats')
        + row.withPanels([
            panels.gauge.memUsage('Memory Usage', queries.memStats),
            panels.gauge.cpuCoreUsage('CPU Usage (1m Avg)', queries.cpuStats),
            panels.gauge.diskUsage('FileSystem Usage', queries.diskStats),
            panels.stat.bytes('Used Memory', queries.usedMem),
            panels.stat.cores('Used Cores', queries.usedCores),
            panels.stat.bytes('Used FileSystem', queries.usedDisk),
            panels.stat.bytes('Total Memory', queries.totalMem),
            panels.stat.cores('Total Cores', queries.totalCores),
            panels.stat.bytes('Total FileSystem', queries.totalDisk),
        ]),
    ], panelWidth=8)
)