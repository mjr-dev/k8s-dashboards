local grafonnet = import 'github.com/grafana/grafonnet/gen/grafonnet-v10.2.0/main.libsonnet';

local var = grafonnet.dashboard.variable;
{
    datasource:
        var.datasource.new('datasource', 'prometheus'),
    cluster:
        var.query.new('cluster', 'label_values(cluster_name)')
        + var.query.withDatasourceFromVariable(self.datasource)
        + var.query.withRefresh('time')
        + var.query.selectionOptions.withIncludeAll(),
    node:
        var.query.new('node')
        + var.query.withDatasourceFromVariable(self.datasource)
        + var.query.queryTypes.withLabelValues(
            'node',
            'kube_node_labels{cluster_name=~"$cluster"}'
        )
        + var.query.withRefresh('time')
        + var.query.selectionOptions.withIncludeAll(),
}